import React from 'react';
//import { Provider } from 'react-redux'
//import configureStore from './configureStore'
import RootNavigation from './navigation/RootNavigation';

//const store = configureStore()

export default class App extends React.Component {
  render() {
    return (
      <RootNavigation />
    );
  }
}
