class Api {
    static headers() {
      return {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'dataType': 'json',
      }
    }
  
    static get(route) {
        const host = 'http://guineefoot.info'
        const url = `${host}${route}`
        return fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
          return responseJson;
        })
        .catch((error) => {
          console.error(error);
        });
    }
  
    static put(route, params) {
      return this.xhr(route, params, 'PUT')
    }
  
    static post(route, params) {
      return this.xhr(route, params, 'POST')
    }
  
    static delete(route, params) {
      return this.xhr(route, params, 'DELETE')
    }
  
    static xhr(route, params, verb) {
      const host = 'http://guineefoot.info'
      const url = `${host}${route}`
      let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null );
      options.headers = Api.headers()
      return fetch(url, options).then( resp => {
        let json = resp.json();
        if (resp.ok) {
          return json
        }
        return json.then(err => {throw err});
      }).then( json => json.results );
    }
  }
  export default Api