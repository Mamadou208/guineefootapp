import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

export default class StartScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  handlePress = () => {
    alert('ik ben binnen');
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}>
          <View style={styles.welcomeContainer}>
            <Image
              source={require('../assets/images/muiswerklogowit.png')
              }
              style={styles.welcomeImage}
            />
          </View>
          <View style={styles.subjectContainer}>
            <TouchableOpacity onPress={this.handlePress} style={[styles.circle, styles.CALColor]} >
            <Icon name="bookmark" size={32} color="#0F6A8C" />
            <Text style={styles.subjectText}>{'Rekenen'.toUpperCase()}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.handlePress} style={[styles.circle, styles.NLColor]} >
            <Icon name="bookmark" size={32} color="#0F6A8C" />
              <Text style={styles.subjectText}>{'Nederlands'.toUpperCase()}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.handlePress} style={[styles.circle, styles.ALLColor]}>
            <Icon name="bookmark" size={32} color="#0F6A8C" />
             <Text style={styles.subjectText}>{'Alle Vakken'.toUpperCase()}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.handlePress} style={[styles.circle, styles.ENColor]}>
            <Icon name="bookmark" size={32} color="#0F6A8C" />
             <Text style={styles.subjectText}>{'English'.toUpperCase()}</Text>
            </TouchableOpacity>
          </View>

        </ScrollView>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f1f9fb',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 250,
    height: 40,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  subjectContainer:{
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  circle: {
    width: 120,
    height: 120,
    borderRadius: 120 / 2,
    alignItems: 'center',
    paddingVertical: 5,
    margin: 10,
    elevation: 5,
  },
  NLColor: {
    backgroundColor: '#59B0C3',
  },
  ENColor: {
    backgroundColor: '#59B0C3',
  },
  CALColor: {
    backgroundColor: '#59B0C3',
  },
  ALLColor: {
    backgroundColor: '#59B0C3',
  },
  subjectText: {
    color: '#0F6A8C',
    paddingVertical: 20,
    fontSize: 20,
    fontWeight: 'bold',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  subjectIcon:{
    color: '#fff'
  }
});
