import styled from 'styled-components/native';
import { Text, Container } from '../../styles';

// You can inherit styled components like so. It keeps all the props of the parent component.
export const Title = styled(Text) `
  margin-bottom: 56px;
`;

export const HeaderContainer =  styled.View `
height: 80px;
align-items: center;
background-color: transparent;
`;

export const NavLogoLeft = styled.View`
background-color: #333;
padding-top: 25px;
padding-horizontal: 15px;
width: 100%;
height: 80px;
`;

export const Logo = styled.Image`
width: 200px;
height: 50px;
`;