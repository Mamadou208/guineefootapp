import React, { Component } from 'react';
import {
    Dimensions,
    Text,
    View,
    TouchableWithoutFeedback,
    Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Container, HeaderContainer, NavLogoLeft, Logo } from './Header.style';
const { width, height } = Dimensions.get('window');

const Header = props => (
    <HeaderContainer>
        <NavLogoLeft>
            <Logo source={require('../../assets/images/guinefoot_logo.png')} />
        </NavLogoLeft>
    </HeaderContainer>
)

export default Header;
