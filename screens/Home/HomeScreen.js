//V170428179
import React, { Component } from 'react'
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  View,
  Image,
  FlatList
} from 'react-native';
import Header from './../Header/Header'
import HeaderStyle from './../../styles'
import Api from './../../api/Api'
import Slide from './Slider'
const { width, height } = Dimensions.get('window');


export default class HomeScreen extends Component {
  constructor(props) {
    super(props)
    this.pagesEndPoint = `/wp-json/wp/v2/pages`;
    this.postsEndPoint = `/wp-json/wp/v2/posts?_embed`;

    this.state = {
      data: [],
      lastPost: [],
      image: [],
    };
  }

  static navigationOptions = {
    header: null,
  };
  handlePress(item) {
    this.props.navigation.navigate('Detail', { item: item });
  }

  componentWillMount() {
    this.fetchData()
  };
  fetchData = () => {
    Api.get(this.postsEndPoint).then((resp) => {
      this.setState({
        data: resp,
      });
    }).catch((ex) => {
      console.log(ex);
    });
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          width: 5,
        }}
      />
    );
  };

  _keyExtractor = (item, index) => item.id;

  _renderItem = ({ item }) => (
    <View style={styles.section}>
      <TouchableOpacity
        onPress={() => this.handlePress(item)} >
        <Image
          style={styles.image}
          source={{ uri: `${item._embedded['wp:featuredmedia'][0].media_details.sizes.medium.source_url}` }}
        />
        <Text style={styles.postTitle}>{item.custom_field.title}</Text>
        <View style={styles.cardFooter}>
        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center' }} >
          <Image style={{ height: 24, width: 24, borderRadius: 12 }} source={{ uri: `${item._embedded['author'][0].avatar_urls[48]}` }} />
          <Text style={styles.author}>{item._embedded['author'][0].name}</Text>
        </View>
        <Text style={styles.author}>12k</Text>
        </View>

      </TouchableOpacity>
    </View>
  );

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          contentContainerStyle={styles.contentContainer}>
          <Header />
          <View style={styles.container}>
            <FlatList
              style={{ marginHorizontal: 15, }}
              data={this.state.data}
              keyExtractor={this._keyExtractor}
              renderItem={this._renderItem}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f6fc',
  },
  contentContainer: {
    paddingTop: 0,
  },
  title: {
    padding: 10,
    fontWeight: 'bold',
    fontSize: 16,
    color: '#212121',
  },
  section: {
    backgroundColor: '#FFFFFF',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderWidth: 1,
    borderColor: '#d6d7da',
    marginTop: 10,
  },
  sectionTitle: {
    color: '#fff',
    fontWeight: '200',
    paddingBottom: 10,
  },
  post: {
    backgroundColor: '#081412',
    marginRight: 10,
  },
  image: {
    width: width,
    height: 200,
    paddingBottom: 20,
  },
  postTitle: {
    color: '#000000',
    fontWeight: '400',
    fontSize: 16,
    padding: 30,
    paddingBottom: 10
  },
  author: {
    color: '#000000',
    fontWeight:'bold',
    fontSize: 14,
    padding: 10,
  },
  cardFooter:{
    marginHorizontal: 20,
    paddingBottom: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
});
