import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    Dimensions
} from 'react-native';

import Swiper from 'react-native-swiper';
import Api from './../../api/Api';

const { width } = Dimensions.get('window');

export default class extends Component {
    constructor(props) {
        super(props)
        this.lastPostEndPoint = `/wp-json/wp/v2/posts?per_page=3&_embed`;
        this.state = {
            imagesSlider: [],
        };
    };

    handlePress(item) {
        this.props.navigation.navigate('Detail');
    };

    componentWillMount() {
        this.getLastPost()
    };

    getLastPost = () => {
        Api.get(this.lastPostEndPoint).then((resp) => {
            this.setState({
                imagesSlider: resp,
            });
        }).catch((ex) => {
            console.log(ex);
        });
    };

    render() {
        return (
            <Swiper
                autoplay
                height={240}
            >
                {
                    this.state.imagesSlider.map((item, key) => {
                        return (
                            <View key={key} style={styles.container}>
                                <Image style={styles.image}
                                    source={{ uri: `${item._embedded['wp:featuredmedia'][0].media_details.sizes.full.source_url}` }}
                                />
                                <Text style={styles.postTitle}>{item.custom_field.title}</Text>
                            </View>
                        )
                    })
                }
            </Swiper>
        )
    };
}
const styles = {
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    image: {
        flex: 1,
        width
    }
};