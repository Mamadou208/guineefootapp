import React, { Component } from 'react'
import {
    Dimensions,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import Header from './../Header/Header'
const { width, height } = Dimensions.get('window');

export default class DetailScreen extends Component {
    
  static navigationOptions = {
    /* ♣ */
    title: 'ARTICLE',
    /* headerStyle: { backgroundColor: '#333' }, 
    headerTitleStyle: { color: 'white', paddingVertical: 40},*/
  };
    render() {
        const { params } = this.props.navigation.state
        return (
            <View style={styles.container}>
                <ScrollView style={styles.container}>
            
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <View style={styles.container}>
                            <Image style={styles.image}
                                source={{ uri: `${params.item._embedded['wp:featuredmedia'][0].media_details.sizes.full.source_url}` }}
                            />
                        </View>
                        <View style={styles.contentContainer}>
                        <Text style={styles.postTitle}>{params.item.custom_field.title}</Text>
                            <Text>{params.item.custom_field.content}</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    contentContainer: {
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    image: {
        width,
        height: 200
    },
    postTitle: {
        color: '#212121',
        fontWeight: 'bold',
        fontSize: 16,
        paddingBottom: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
});